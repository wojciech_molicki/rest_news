import datetime

from django.utils import timezone
from django.db import models

class ApiLog(models.Model):
	access_datetime = models.DateTimeField('access datetime')
	total_result_count = models.IntegerField(default = 0)
	sources_with_result_count = models.IntegerField(default = 0)
	user_ip = models.CharField(max_length=45)
	user_query = models.CharField(max_length=100)

	def getFormattedDateAsString():
		return self.access_datetime.strftime('%Y.%m.%d %H:%M:%S')
	
	def __str__(self):
		return str(' '.join([str(self.id),
			self.getFormattedDateAsString(),
			self.user_ip]))
