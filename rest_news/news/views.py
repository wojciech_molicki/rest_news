import json

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect, HttpResponseNotFound
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from news.models import ApiLog
from news.search_providers import SearchProviders

def index(request):
	# pagination limits logs to PAGINATOR_MAX_COUNT
	log_list = ApiLog.objects.all().order_by('-id')
	paginator = Paginator(log_list, settings.PAGINATOR_MAX_COUNT)
	page = request.GET.get('page')

	try:
		logs = paginator.page(page)
	except PageNotAnInteger:
		logs = paginator.page(1)
	except EmptyPage:
		logs = paginator.page(paginator.num_pages)

	return render(request, 'news/index.html', {'logs' : logs})

def getResultDict(query):
	sp = SearchProviders()
	d = sp.build_result_dict(query)
	return d

def addLog(query, result_dict, user_ip):
	'''
	adds new entry to db
	'''
	l = ApiLog(access_datetime = timezone.now(),
				total_result_count = len(result_dict['results']),
				sources_with_result_count = getResultsSourcesNumber(result_dict),
				user_ip = str(user_ip),
				user_query = query,
		)
	l.save()

def getResultsSourcesNumber(result_dict):
	'''
	returns number of different data sources in result_dict
	'''
	return len(set([i['dataProvider'] for i in result_dict['results']]))

def search(request):
	if 'q' in request.GET and request.GET['q'] != '':
		query = request.GET['q']
	else:
		return HttpResponseNotFound(
			json.dumps({ 'error': 'Not found' }),
			 content_type="application/json")
	
	try:
		user_ip = request.META['REMOTE_ADDR']
	except KeyError:
		user_ip = 'unknown'

	result_dict = getResultDict(query)	
	addLog(query, result_dict, user_ip)

	return HttpResponse(json.dumps(result_dict), content_type="application/json")




def api(request):
	d = {
		'url' : '/news/api/v1.0',
		'resources' : {
			'search' : {
				'url' : '/search'
			}
		}
	}
	return HttpResponse(json.dumps(d), content_type="application/json")



