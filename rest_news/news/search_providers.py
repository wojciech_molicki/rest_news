import json
import urllib
import urllib.request, urllib.parse, urllib.error

from news.deploy import api_keys, server_ip

class SearchProviders:
	def get_bing_search_url(self, query):
		root_url = 'https://api.datamarket.azure.com/Bing/Search/'
		source = 'News'
		market = "'pl-PL'"

		max_results = 20

		query = "'{}'".format(query)
		query = urllib.parse.quote(query)

		search_url = "{0}{1}?$format=json&$top={2}&Query={3}&Market={4}".format(
			root_url, source, max_results, query, market)
		return search_url

	def bing_search(self, query):
		'''
		returns news dictionary
		'''
		search_url = self.get_bing_search_url(query)
		api_key = api_keys['bing']

		password_manager = urllib.request.HTTPPasswordMgrWithDefaultRealm()
		password_manager.add_password(None, search_url, '', api_key)

		as_dict = {}

		try:	
			handler = urllib.request.HTTPBasicAuthHandler(password_manager)
			opener = urllib.request.build_opener(handler)
			urllib.request.install_opener(opener)

			url_handle = urllib.request.urlopen(search_url)

			charset = url_handle.info().get_param('charset', 'utf8')
			response = url_handle.read()
			as_dict = json.loads(response.decode(charset))
		except urllib.URLError as e:
			print("Error while querying Bing API", e)
		return as_dict

	def get_google_search_url(self, query):
		root_url = 'https://ajax.googleapis.com/ajax/services/search/news'
		ip = server_ip
		version = '1.0'
		ned = 'pl_pl'

		query = "'{}'".format(query)
		query = urllib.parse.quote(query)

		search_url = "{0}?v={1}&ip={2}&q={3}&ned={4}".format(
			root_url, version, ip, query, ned)
		return search_url

	def google_search(self, query):
		'''
		returns news dictionary
		'''
		search_url = self.get_google_search_url(query)
		as_dict = {}

		try:
			url_handle = urllib.request.urlopen(search_url)
			charset = url_handle.info().get_param('charset', 'utf8')
			response = url_handle.read()
			as_dict = json.loads(response.decode(charset))
		except urllib.error.URLError as e:
			print("Error while querying Google API", e)
		return as_dict

	def bing_list_fill(self, target_list, query):
		'''
		given target list and query, append bing search results to 
		target list in a correct format
		'''
		d = self.bing_search(query)
		results = d['d']['results']
		for result in results:
			target_list.append({
					'date' : result['Date'],
					'url' : result['Url'],
					'title' : result['Title'],
					'source' : result['Source'],
					'content' : result['Description'],
					'dataProvider' : 'bing',
			})

	def google_list_fill(self, target_list, query):
		'''
		given target list and query, append google search results to 
		target list in a correct format
		'''
		d = self.google_search(query)
		results = d['responseData']['results']
		for result in results:
			target_list.append({
					'date' : result['publishedDate'],
					'url' : result['unescapedUrl'],
					'title' : result['titleNoFormatting'],
					'source' : result['publisher'],
					'content' : '',
					'dataProvider' : 'google',
			})

	def build_result_dict(self, query):
		'''
		goes over the list of providers, and fills response list
		'''
		providers_fillers = [self.bing_list_fill,
			self.google_list_fill]

		response_list = []

		for provider in providers_fillers:
			provider(response_list, query)

		return {'results' : response_list}

