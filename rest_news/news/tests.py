import datetime
import json

from django.utils import timezone
from django.test import TestCase
from django.test import Client
from django.test.utils import setup_test_environment
from django.conf import settings
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from unittest.mock import patch, Mock, MagicMock

from news.models import ApiLog
from news.search_providers import SearchProviders
from news.views import getResultsSourcesNumber, addLog, getResultDict

from news.google_results import google_results
from news.bing_results import bing_results

class ModelTests(TestCase):
	def test_add_empty_result(self):
		empty_result = {'results': []}
		addLog('query', empty_result, '1.2.3.4')
		entry = ApiLog.objects.get(id=1)
		self.assertEqual(entry.user_ip, '1.2.3.4')
		self.assertEqual(entry.sources_with_result_count, 0)
		self.assertEqual(entry.user_query, 'query')
		self.assertEqual(entry.total_result_count, 0)

class JsonTests(TestCase):
	def setUp(self):
		setup_test_environment()
		self.client = Client()

	def test_no_query_parameter(self):
		response = self.client.get('/news/api/v1.0/search/')
		self.assertEqual({ 'error': 'Not found' },
		 json.loads((response.content).decode('utf-8')))

	def test_empty_query_parameter(self):
		response = self.client.get('/news/api/v1.0/search/')
		self.assertEqual({ 'error': 'Not found' },
		 json.loads((response.content).decode('utf-8')))

class PaginationTests(TestCase):
	def populateDb(self):
		'''
		add items over pagination max count
		'''
		setup_test_environment()
		self.client = Client()
		
		for i in range(1, settings.PAGINATOR_MAX_COUNT+2):
			if i == 1:
				uq = 'not_visible'
			if i == settings.PAGINATOR_MAX_COUNT+1:
				uq = 'visible'
			else:
				uq = ''
			l = ApiLog(access_datetime = timezone.now() +
				datetime.timedelta(minutes=i),
				total_result_count = i,
				sources_with_result_count = 0,
				user_ip = '0.0.0.0',
				user_query = uq,
			)
			l.save()

	def test_pagination_hiding_items_over_max_count(self):
		'''
		check if oldest item is not visible
		'''
		self.populateDb()
		response = self.client.get('/news/')
		#print(response.decode('utf-8'))
		self.assertNotContains(response, 'not_visible')

	def test_pagination_not_hiding_newest_item(self):
		'''
		check if newest item is visible
		'''
		self.populateDb()
		response = self.client.get('/news/')
		self.assertContains(response, 'visible')

	def test_pagination_non_integer_page(self):
		'''
		check if non integer page param redirects to first page
		'''
		self.populateDb()
		response = self.client.get('/news/?page=mmm')
		self.assertContains(response, 'visible')

	def test_pagination_overflow(self):
		'''
		check if too big page number redirects to last page
		'''
		self.populateDb()
		response = self.client.get('/news/?page=1500')
		self.assertNotContains(response, 'visible')

class SearchProvidersTests(TestCase):
	def setUp(self):
		self.google_empty_results = \
		{'responseData': 
			{'results': [], 'cursor': 
				{'moreResultsUrl': ""}
			},
			'responseDetails': None,
			'responseStatus': 200
		}

		self.bing_empty_results = {'d': {'results': []}}


	def test_return_types_with_mocked_bing_search_and_google_search(self):
		'''
		check whether mocked methods return correct data types
		'''
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results
		
		self.assertEqual(type(real.bing_search('')), type({}))
		self.assertEqual(type(real.google_search('')), type({}))
		self.assertEqual(type(real.build_result_dict('')), type({}))


	def test_output_dict_length(self):
		'''
		check results list length using mocked search results
		'''
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results

		self.assertEqual(
			len(real.build_result_dict('query')['results']), 19
		)

	def test_results_with_empty_provider_results(self):
		'''
		check result list length and content using empty search results
		'''
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = self.bing_empty_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = self.google_empty_results

		self.assertEqual(
			len(real.build_result_dict('query')['results']), 0
		)
		self.assertEqual(real.build_result_dict('query'), {'results': []})

	def test_bing_list_fill(self):
		'''
		'''
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results
		target_list = []
		results_len = len(bing_results['d']['results'])
		real.bing_list_fill(target_list, '')

		self.assertEqual(len(target_list), results_len)

	def test_google_list_fill(self):
		'''
		'''
		real = SearchProviders()
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results
		target_list = []
		results_len = len(google_results['responseData']['results'])
		real.google_list_fill(target_list, '')

		self.assertEqual(len(target_list), results_len)

	def test_list_fill_valid_format(self):
		'''
		check if result contains dictionaries 
		 with correct keys (example):
		{
			'date' : result['publishedDate'],
			'url' : result['unescapedUrl'],
			'title' : result['titleNoFormatting'],
			'source' : result['publisher'],
			'content' : '',
			'dataProvider' : 'google',
		}
		'''
		real = SearchProviders()
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results

		d = real.build_result_dict('')
		correct_keys = ['date', 'url', 'title', 'source',
						 'content', 'dataProvider']
		for entry in d['results']:
			entry_keys = entry.keys()
			self.assertEqual(set(entry_keys) & set(correct_keys),
								set(correct_keys))

	def test_build_result_dict(self):
		'''
		'''
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results
		target_list = []
		google_results_len = len(google_results['responseData']['results'])
		bing_results_len = len(bing_results['d']['results'])

		self.assertEqual(len(real.build_result_dict('')['results']),
		 					google_results_len + bing_results_len)

	def test_get_google_search_url(self):
		query = 'test query xxx "'
		real = SearchProviders()
		search_url = real.get_google_search_url(query)
		self.assertEqual(' ' not in search_url, True)
		self.assertEqual('"' not in search_url, True)
		self.assertEqual('test%20query%20xxx' in search_url, True)

	def test_get_bing_search_url(self):
		query = 'test query xxx "'
		real = SearchProviders()
		search_url = real.get_bing_search_url(query)
		self.assertEqual(' ' not in search_url, True)
		self.assertEqual('"' not in search_url, True)
		self.assertEqual('test%20query%20xxx' in search_url, True)

	def test_bing_validate_url(self):
		query = "some query with *&#! naughty letters"
		real = SearchProviders()
		search_url_bing = real.get_bing_search_url(query)

		validator = URLValidator()
		valid = True
		try:
			validator(search_url_bing)
		except ValidationError:
			valid = False

		self.assertEqual(valid, True)
		
	def test_google_validate_url(self):
		query = "some query with *&#! naughty letters"
		real = SearchProviders()
		search_url_google = real.get_google_search_url(query)

		validator = URLValidator()
		valid = True
		try:
			validator(search_url_google)
		except ValidationError:
			valid = False

		self.assertEqual(valid, True)

class ResourcesSourcesNumberTests(TestCase):
	def setUp(self):
		

		self.google_empty_results = \
		{'responseData': 
			{'results': [], 'cursor': 
				{'moreResultsUrl': ""}
			},
			'responseDetails': None,
			'responseStatus': 200
		}

		self.bing_empty_results = {'d': {'results': []}}

	def test_get_resources_sources_number_with_bing_only(self):
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = self.google_empty_results

		d = real.build_result_dict('')

		self.assertEqual(getResultsSourcesNumber(d), 1)

	def test_get_resources_sources_number_with_google_only(self):
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = self.bing_empty_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results

		d = real.build_result_dict('')

		self.assertEqual(getResultsSourcesNumber(d), 1)

	def test_get_resources_sources_number_with_both_sources(self):
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = bing_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = google_results

		d = real.build_result_dict('')

		self.assertEqual(getResultsSourcesNumber(d), 2)

	def test_get_resources_sources_number_with_no_sources(self):
		real = SearchProviders()
		real.bing_search = MagicMock(name='bing_search')
		real.bing_search.return_value = self.bing_empty_results
		real.google_search = MagicMock(name='google_search')
		real.google_search.return_value = self.google_empty_results

		d = real.build_result_dict('')

		self.assertEqual(getResultsSourcesNumber(d), 0)








