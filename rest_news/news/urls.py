from django.conf.urls import patterns, url
from news import views

urlpatterns = patterns('',
	# /news/
	url(r'^$', views.index, name = 'index'),

	# /news/api/
	url(r'^api/$', views.api, name = 'api'),

	# /news/api/query/query_text
	url(r'^api/v1.0/search/?$', views.search, name = 'search'),

)